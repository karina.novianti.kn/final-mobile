<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Create Deposit</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2db4d668-49cc-4716-90dc-8108632f4e66</testSuiteGuid>
   <testCaseLink>
      <guid>feec85ef-50d5-4bd5-b2c2-8a86764e0197</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2 Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ec1b574-3745-4c61-8485-a19661f5928a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/3 Create Account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c14f7a84-ec1f-445b-8c7e-352d55f3a71d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/4 Make a deposit</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
